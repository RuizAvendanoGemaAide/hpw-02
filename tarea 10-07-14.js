/*Profesor*/
obj_profesor =
[
	{
		"clave":"HBA896",
		"nombre":"Hernandez Blas Antonio"
	}
];

/*Materias*/
obj_materia1 = [
	{
		"clave":"m1",
		"nombre":"Matematicas 1"
	}];
obj_materia2 = [	{
		"clave":"m2",
		"nombre":"Español"
	}];
obj_materia3 = [	{
		"clave":"m3",
		"nombre":"Historia"
	}];
obj_materia4 = [	{
		"clave":"m4",
		"nombre":"Geografia"
	}];
obj_materia5 = [{
		"clave":"m5",
		"nombre":"Civismo"
	}];
obj_materia6 = [	{
		"clave":"m6",
		"nombre":"Biologia"
	}];
obj_materia7 = [	{
		"clave":"m7",
		"nombre":"Educacion fisica"
	}
];

/*Alumnos*/
obj_alumnos = 
[
	{	
		"ncontrol":"09161133",
		"nombre":"Banchez Cruz Pablo Zaid",
		"grupo": "A"
	},
	{
		"ncontrol":"09161278",
		"nombre":"Gema Aidé Ruiz Avendaño",
		"grupo": "A"
	},
	{
		"ncontrol":"09161135",
		"nombre":"Luis Garcia Postiga",
		"grupo": "A"
	}
];

/*Horarios*/
obj_horarios =
[
	{
		"clave":"h1",
		"hora_inicial":"12:00",
		"hora_final":"13:00",
		"materia":obj_materia1["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h2",
		"hora_inicial":"13:00",
		"hora_final":"14:00",
		"materia":obj_materia2["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h3",
		"hora_inicial":"14:00",
		"hora_final":"15:00",
		"materia":obj_materia3["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h4",
		"hora_inicial":"15:00",
		"hora_final":"16:00",
		"materia":obj_materia4["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h5",
		"hora_inicial":"16:00",
		"hora_final":"17:00",
		"materia":obj_materia5["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h6",
		"hora_inicial":"17:00",
		"hora_final":"18:00",
		"materia":obj_materia6["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h7",
		"hora_inicial":"19:00",
		"hora_final":"20:00",
		"materia":obj_materia7["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	}
];

/*Grupo*/
obj_grupo =
{
	"clave":"g1",
	"nombre":"A",
	"alumnos": obj_alumnos,
	"materias": [obj_materia1,obj_materia2,obj_materia3,obj_materia4,obj_materia5,
	obj_materia6,obj_materia7],
	"profesores": obj_profesor,
	"horarios": obj_horarios
};


/*insertar un alumno*/
function insertar_alumno(){
    var control;
    var nombre;
    var grupo;
    control=prompt('No. Control:','');
    nombre=prompt('Ingrese Nombre:','');
    grupo=prompt('Ingrese Grupo:','');
    console.log(control);
    console.log(nombre);
    console.log(grupo);
    obj_alumnos.push(obj_alumnos["ncontrol"]=control,obj_alumnos["nombre"]=nombre,obj_alumnos["grupo"]=grupo);
}

insertar_alumno();

obj_alumnos;

/*Insertar un Maestro*/
function insertar_maestro(){
    var clave;
    var nombre;
    var horai;
    var horaf;
    clave=prompt('Clave:','');
    nombre=prompt('Ingrese Nombre:','');
    horai=prompt('Hora inicial: ', '')
    horaf=prompt('Hora Final')
    console.log(clave);
    console.log(nombre);
    console.log(horai);
    console.log(horai);
    obj_profesor.push(obj_profesor["clave"]=clave,obj_profesor["nombre"]=nombre);
    obj_horarios.push(obj_horarios["clave"]=clave, obj_horarios["profesor"]=clave,obj_horarios["hora_inicial"]=horai,obj_horarios["hora_final"]=horaf);
}

insertar_maestro();
obj_profesor;
obj_horarios;


/*Inserta una materia*/


function insertar_materia(){
    var clave;
    var nombre;
    var horai;
    var horaf;
    var grupo;
    clave=prompt('Clave:','');
    nombre=prompt('Ingrese Nombre:','');
    console.log(clave);
    console.log(nombre);
    obj_materia1.push(obj_materia1["clave"]=clave,obj_materia1["nombre"]=nombre);
    obj_horarios.push(obj_horarios["clave"]=clave, obj_horarios["profesor"]=clave,obj_horarios["hora_inicial"]=horai,obj_horarios["hora_final"]=horaf,obj_horarios["grupos"]=grupo);
    obj_grupo.push(obj_grupo["grupos"]=grupo)
}

insertar_materia();
obj_materia1;

/*  BORRAR    */

/*  ACTUALIZAR   */

function actualizarAlumno(){ 
    var indice;
    var nuevo;
    indice=prompt('Dame indice:','');
    alert('El elemento contiene '+obj_alumnos[indice]+'.')
    nuevo=prompt('Dame un nuevo valor:','');
    obj_alumnos[indice]=nuevo;
    alert('El elemento contiene '+obj_alumnos[indice]+'.')
}

actualizarAlumno();

obj_alumnos;

/*   LEER     */

 function horario_profesor_materia(grupo){
	if (grupo["horarios"])	{
		for (i=0; i < grupo["horarios"].length; i++)		{
			console.log(grupo["horarios"][i]["hora_inicial"]);
		}
	}
	if (grupo["profesores"])	{
		for (i=0; i < grupo["profesores"].length; i++)
		{
			console.log(grupo["profesores"][i]["nombre"]);
		}
	}
	if (grupo["horarios"])	{
		for (i=0; i < grupo["horarios"].length; i++)
		{
			console.log(grupo["horarios"][i]["materia"]);
		}
	}
	
}
 
horario_profesor_materia(obj_grupo);
 